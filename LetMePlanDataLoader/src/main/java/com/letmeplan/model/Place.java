package com.letmeplan.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
@IdClass(PlaceCompositeKey.class)
public class Place implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private String source; 

	@Id
	private String id ; 

	private String type ;
	
	private String name ;
	private String description ;
	private String address ;
	private String phoneNumber ;
	private String hours ;
	private Double duration ;
	private Double rating ;
	private Double cost  ;
	private Double latitude ;
	private Double longitude ;
	
	private String region ;
	private String locality ;
	private String postcode ;
	private String website ;
	
	private String rawData ;

	private String googlePlaceId ;
	
	private String googleType ;
	
	private Boolean validateGoogle ;
	
	private String googleName ;
	private String googleAddress ;
	private Double googleLat ;
	private Double googleLong ;
	private String googleWebsite ;
	private String googlePhone ;
	private Double googleRating ;
	private Integer googleNoOfReviwer ;
	private String googleOpenHours ;
	private String googleError ;
	
	private String meName ;
	private String meCategory ;
	
	
	public Place(){
		
	}

	public Place(String id,String name){
		this.id = id ;
		this.name = name ;
	}
	
	public String getSource() {
		return source;
	}
	
	public void setSource(String source) {
		this.source = source;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getRawData() {
		return rawData;
	}
	
	public void setRawData(String rawData) {
		this.rawData = rawData;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getHours() {
		return hours;
	}
	public void setHours(String hours) {
		this.hours = hours;
	}
	public Double getDuration() {
		return duration;
	}
	public void setDuration(Double duration) {
		this.duration = duration;
	}
	public Double getRating() {
		return rating;
	}
	public void setRating(Double rating) {
		this.rating = rating;
	}
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public String getGooglePlaceId() {
		return googlePlaceId;
	}
	public void setGooglePlaceId(String googlePlaceId) {
		this.googlePlaceId = googlePlaceId;
	}
	public String getGoogleType() {
		return googleType;
	}
	public void setGoogleType(String googleType) {
		this.googleType = googleType;
	}
	public Boolean getValidateGoogle() {
		return validateGoogle;
	}
	public void setValidateGoogle(Boolean validateGoogle) {
		this.validateGoogle = validateGoogle;
	}
	public String getGoogleAddress() {
		return googleAddress;
	}
	public void setGoogleAddress(String googleAddress) {
		this.googleAddress = googleAddress;
	}
	public String getGoogleWebsite() {
		return googleWebsite;
	}
	
	public void setGoogleWebsite(String googleWebsite) {
		this.googleWebsite = googleWebsite;
	}
	
	public String getGooglePhone() {
		return googlePhone;
	}
	
	public void setGooglePhone(String googlePhone) {
		this.googlePhone = googlePhone;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getGoogleName() {
		return googleName;
	}

	public void setGoogleName(String googleName) {
		this.googleName = googleName;
	}

	public Integer getGoogleNoOfReviwer() {
		return googleNoOfReviwer;
	}

	public void setGoogleNoOfReviwer(Integer googleNoOfReviwer) {
		this.googleNoOfReviwer = googleNoOfReviwer;
	}

	public void setGoogleRating(Double googleRating) {
		this.googleRating = googleRating;
	}

	public Double getGoogleLat() {
		return googleLat;
	}

	public void setGoogleLat(Double googleLat) {
		this.googleLat = googleLat;
	}

	public Double getGoogleLong() {
		return googleLong;
	}

	public void setGoogleLong(Double googleLong) {
		this.googleLong = googleLong;
	}

	public String getGoogleError() {
		return googleError;
	}

	public void setGoogleError(String googleError) {
		this.googleError = googleError;
	}

	public void setMeName(String meName) {
		this.meName = meName;
	}

	public void setMeCategory(String meCategory) {
		this.meCategory = meCategory;
	}

	public String getGoogleOpenHours() {
		return googleOpenHours;
	}

	public void setGoogleOpenHours(String googleOpenHours) {
		this.googleOpenHours = googleOpenHours;
	}
	
}
