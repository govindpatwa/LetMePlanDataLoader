package com.letmeplan.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.factual.driver.Factual;
import com.factual.driver.Query;
import com.factual.driver.ReadResponse;
import com.letmeplan.model.Place;
import com.letmeplan.repository.CategoryRepository;
import com.letmeplan.repository.PlaceRepository;


@Service
public class DataLoaderFactualService {

	private static final double DURATION = -1.00;
	private static final String NOT_AVAILABLE = "Not available";
	private static final String MY_KEY = "lAlUIp76lvzGd1ouwvNNiYVYE51WxWEdN45JVPyG";
	private static final String MY_SECRET = "Evi3KaRXIcAuPpGsJNeVkK5Ejg6tm6KhfdlXlkFB";
	
	private static Logger log = LoggerFactory.getLogger(DataLoaderFactualService.class);
	private Factual factual = new Factual(MY_KEY, MY_SECRET);

	private static Integer[] landMarksIds = {107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,461,372,308,317} ;
	
	//private static Integer[] landMarksIds = {107} ;
	
	@Resource
	private PlaceRepository factualPlaceRepository ;

	@Resource
	private CategoryRepository categoryRepository ;

	
	@PostConstruct
	public void loadCategory(){
		
	}
	
	public ReadResponse loadFactualData(String category,String city,String state,String country) throws Exception {
		
/*		
 		String urlStr = "http://api.v3.factual.com/t/places-us?filters={%22$and%22:[{%22country%22:{%22$eq%22:%22US%22}},{%22region%22:{%22$eq%22:%22CA%22}},{%22locality%22:{%22$eq%22";
		urlStr= urlStr + "%22:%22SAN+FRANCISCO%22}},{%22category_labels%22:{%22$includes%22:%22Landmarks%22}}]}&KEY=lAlUIp76lvzGd1ouwvNNiYVYE51WxWEdN45JVPyG";
*/
		List<String> categories = categoryRepository.findByActive(true);
		
		
		String tableName = "places-us";
	    Query q = new Query();
	    q.and(
	        q.field("country").isEqual(country),
	        q.field("region").isEqual(state),
	        q.field("locality").isEqual(city),
	        q.field("category_labels").includesAny(categories)
	    ); 
	    q.includeRowCount();
	    q.limit(50);
	    return factual.fetch(tableName, q);
	}
	
	
	public Integer loadData(String category,String city,String state,String country) throws Exception {
		String tableName = "places-us";
		ReadResponse resp =  null ;
		Integer totalCount =  0;
		List<String> categories = categoryRepository.findByActive(true);
	    for (String id : categories) {
	    	long offset = 0;
	    	Query q = new Query();
		    q.and(
		        q.field("country").isEqual(country),
		        q.field("region").isEqual(state),
		        q.field("locality").isEqual(city),
		        q.field("category_ids").isEqual(id)
		    ); 
		    // include row count in the first call only
		    q.limit(50);
		    try {
		    	resp =  callFactualDataRecursive(tableName, offset, q);
		    } catch (Exception e) {
		    	System.out.println(" Error cat "  + id);
		    	throw new Exception(e); 
		    }
		    totalCount = totalCount + resp.getTotalRowCount();
		   // break ;
		   
	    }
	    return totalCount;
	}


	private ReadResponse  callFactualDataRecursive(String tableName, long offset, Query q) {
		
		q.includeRowCount();
		//if (offset == 0)   q.includeRowCount();
		//else q.includeRowCount(false);
		
		q.offset(offset);
		ReadResponse resp =  factual.fetch(tableName, q);
		offset = offset + resp.size() ;
		List<Map<String, Object>> data = resp.getData();
		List<Place> places =  new ArrayList<Place>();
		for (Map<String,Object> d : data) {
			Place factualPlace = new Place();
			factualPlace.setSource("FACTUAL");
			factualPlace.setId((String) d.get("factual_id"));
			factualPlace.setRawData(d.toString());
			
			if (d.get("category_labels") != null) factualPlace.setType(d.get("category_labels").toString());
			if (d.get("name") != null) factualPlace.setName(d.get("name").toString());
			//factualPlace.setDescription(NOT_AVAILABLE);
			if (d.get("address") != null) factualPlace.setAddress(d.get("address").toString());
			if (d.get("tel") != null) factualPlace.setPhoneNumber(d.get("tel").toString());
			if (d.get("hours") != null) factualPlace.setHours(d.get("hours").toString());
			
			if (d.get("locality") != null) factualPlace.setLocality(d.get("locality").toString());
			if (d.get("postcode") != null) factualPlace.setPostcode(d.get("postcode").toString());
			if (d.get("region") != null) factualPlace.setRegion(d.get("region").toString());
			if (d.get("website") != null) factualPlace.setWebsite(d.get("website").toString());
			

			//factualPlace.setDuration(DURATION);
			//factualPlace.setCost(DURATION);
			factualPlace.setLatitude((Double) d.get("latitude"));
			factualPlace.setLongitude((Double) d.get("longitude"));
			
			places.add(factualPlace);
			d.remove("factual_id");
		}
		factualPlaceRepository.save(places);
		if (!resp.getData().isEmpty() &&  (offset <= 450)) {
			callFactualDataRecursive(tableName,offset,q);
		}
		return resp ;
	}
}