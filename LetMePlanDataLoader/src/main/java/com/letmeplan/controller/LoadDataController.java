package com.letmeplan.controller;

import javax.annotation.Resource;

import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.letmeplan.service.DataLoaderService;

@Controller
public class LoadDataController {

	private HttpRequest httpRequest ;
	
	@Resource
	private DataLoaderService  dataLoaderService ;
	
	@RequestMapping(value="/factual/data",method = RequestMethod.GET)
	public String getData() throws Exception{
		return "factual" ;
	}
	
	@RequestMapping(value="/data/load",method = RequestMethod.POST)
	public @ResponseBody Integer loadData(String country,String state, String city) throws Exception{
		Integer totalCount = dataLoaderService.initDataLoader(null, city, state, country);
		return totalCount ;
	}
}

