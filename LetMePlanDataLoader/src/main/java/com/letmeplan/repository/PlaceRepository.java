package com.letmeplan.repository;

import java.util.List;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.letmeplan.model.Place;
import com.letmeplan.model.PlaceCompositeKey;

@Repository
public interface PlaceRepository  extends GenericDao<Place, PlaceCompositeKey> {

	@Query("select p.id,p.name from Place p")
	public  Page<Place> findByGooglePlaceIdIsNull(Pageable pageable);
	
	//@Query("select new com.letmeplan.model.Place(p.id,p.name) from Place p")
	public  Page<Place> findByValidateGoogleIsFalseOrValidateGoogleIsNull(Pageable pageable);
	
	@Modifying
	@Query("update Place p set p.googlePlaceId = ?1,p.googleType = ?2,p.validateGoogle = ?3,p.googleAddress = ?4,p.googleWebsite= ?5,p.googlePhone = ?6,googleRating = ?7,googleNoOfReviwer = ?8,googleLat = ?9,googleLong = ?10,googleName = ?11  where p.source = ?12 and p.id = ?13")
	@Transactional(value=TxType.REQUIRES_NEW)
	public int updateGoogleData(String gplcId,String gType,Boolean validateGoogle,String address,String gPhone,String website,Double rating,Integer noOfReviewer,Double gLat,Double gLong,String gName,String source, String id);
	
	
	public List<Place> findByGooglePlaceId(String pName);
	
}
